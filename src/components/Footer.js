import React from 'react';
import Button from 'react-bootstrap/Button';

const Footer = () => {
    return (
        <div>
             <Button variant="primary">Primary</Button>{' '}
      <Button variant="secondary">Secondary</Button>{' '}
      <Button variant="success">Success</Button>{' '}
      <Button variant="warning">Warning</Button>{' '}
      <Button variant="danger">Danger</Button>{' '}
        </div>
    );
}

export default Footer;
