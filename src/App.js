import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Formulaire from './components/Formulaire';
import Footer from './components/Footer';

function App() {
  return (
    <div className="App">
       <Header></Header>
     <Formulaire></Formulaire>
     <Footer></Footer>
    </div>
  );
}

export default App;
